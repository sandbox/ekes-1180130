<?php

/**
 * @file
 *   Messaging VOIP scripts, and related script theme functions.
 */

/****
 * Script callbacks.
 */

/**
 * Script to send a single message.
 */
function messaging_voip_script_message_send($message) {
  $script = new VoipScript('messaging_voip_script_message_send');
  $script->setVar('introduction', theme('messaging_voip_message_send_intro'));
  $script->setVar('message', $message);
  $script->setVar('goodbye', theme('messaging_voip_message_send_end'));

  $script->addSay('%introduction');
  $script->addLabel('play message');
  $script->addSay('%message');
  $script->addLabel('get input');
  $text = t('To play this message again press 1. To hangup, press the # key.');
  $timeout = 5;
  $end_key = '';
  $num_digits = 1;
  $script->addGetInput($text, $num_digits, $end_key, $timeout);
  $script->addGotoIf('play message', "^%input_digits == '1'");
  $script->addGotoIf('end call', "^%input_digits' == '#'");
  // Wrong input
  $script->addSay(t('Invalid input received. Please try again.'));
  $script->addGoto('get input');
  // Terminate Call
  $script->addLabel('end call');
  $script->addSay('%goodbye');
  $script->addHangup();

  return $script;
}

/**
 * Script to call out with messages in the index.
 */
function messaging_voip_script_croncall($account) {
  if (! is_object($account)) {
    $account = user_load($account);
  }

  $script = new VoipScript('messaging_voip_script_croncall');
  $script->setVar('introduction', theme('messaging_voip_message_send_intro', $account));
  $script->setVar('goodbye', theme('messaging_voip_message_send_end'));

  $script->addSay('%introduction');
  $script->addGosub('messaging_voip_script_store_index', array('account' => $account->uid));
  $script->addSay('%goodbye');
  $script->addHangUp();

  return $script;
}

/**
 * Index message, option to read or return, must be called with messages.
 * 
 * @param $account
 *   User object.
 * @param $options
 *   array return_option => array('1', 'action');
 *         read_callback => script to read messages // complicating api - delete?
 */
function messaging_voip_script_store_index($account, $options = NULL) {
  if (! is_object($account)) {
    $account = user_load($account);
  }
  // default options.
  $return_option = empty($options['return_option']) ? array('#' => 'Press # to return.') : $options['return_option'];
  $read_callback = empty($options['read_callback']) ? 'messaging_voip_script_store_read' : $options['read_callback'];

  $script = new VoipScript('messaging_voip_script_store_index');
  $options = $return_option;
  $messages = messaging_voip_store_get($account);
  $count = count($messages);

  if ($count) {
    $script->setVar('index', theme('messaging_voip_store_index', count($messages), $messages));
    $options = array('1' => t('To play, press 1.')) + $options;
  }
  else {
    $script->setVar('index', theme('messaging_voip_store_nomessages'));
  }

  $script->addLabel('get input');
  $script->addSay('%index');
  $text = implode('', $options);
  $timeout = 5;
  $end_key = '';
  $num_digits = 1;
  $script->addGetInput($text, $num_digits, $end_key, $timeout);
  foreach ($options as $key => $option) {
    $script->addGotoIf('result input ' . $key, "^%input_digits == '$key'");
  }
  // Invalid input
  $script->addSay('Invalid input received. Please try again.');
  $script->addGoto('get input');

  // 1 => To play, press 1.
  $script->addLabel('result input 1');
  $script->addGosub('messaging_voip_script_store_read', array('account' => $account->uid));
  $script->addGoto('get input');

  // Finish
  $script->addLabel('result input ' . key($return_option));
  $script->addReturn();

  return $script;
}

function messaging_voip_script_store_read($account) {
  if (! is_object($account)) {
    $account = user_load($account);
  }
  $script = new VoipScript('messaging_voip_script_store_read');
  // @todo thinking: the script has to know about all messages, it can't paginate, it has to send everything off in the script? no callback for more? interact?
  $messages = messaging_voip_store_get($account);
  if (! count($messages)) {
    $script->addSay(theme('messaging_voip_store_nomessages'));
    $script->addReturn();
    return $script;
  }

  /**
   * Loop through the messages and make a script.
   *
   * If I understand it correctly don't even think of trying to go backwards as well this way.
   */
  $index = array_keys($messages); $count = 0;
  while ($count < count($index)) {
    $mqid = $index[$count];
    $script->addLabel('play message ' . $count);
    $script->addSay($messages[$mqid]->subject);
    $script->addSay($messages[$mqid]->body);

    $script->addLabel('get input');
    $timeout = 5; $end_key = ''; $num_digits = 1;
    $text = t('To play this message again press 1.');
    if ($count + 1 < $count($index)) {
      $text .= t('To play the next message press 2.');
    }
    $text .= t('To return to previous menu press #.');
    $script->addGetInput($text, $num_digits, $end_key, $timeout);
    $script->addGotoIf('play message ' . $count, "^%input_digits == '1'");
    if ($count +1 < $count($index)) {
      $script->addGotoIf('next message ' . $count, "^%input_digits == '2'");
    }
    $script->addGotoIf('return', "^%input_digits == '#'");
    // Invalid input
    $script->addSay('Invalid input received. Please try again.');
    $script->addGoto('get input');

    $script->addLabel('next message');*/
    $script->addLabel('next message ' . $count);
    $count++;
  }

  $script->addLabel('return');
  $script->addReturn();

  return $script;
}

/**
 * Script if incoming calls are handled only for collecting the notifications.
 */
function messaging_voip_script_store_incoming($params = NULL) {
  $script = new VoipScript('messaging_voip_script_store_incoming');

  $script->addSay('This is in debug mode. You are automatically becoming user 1');
  $script->addSay('Handing to cron call script.');
  // @todo why can I only pass strings?
  $script->addGosub('messaging_voip_script_croncall', array('account' => 1));

  return $script;
}

/****
 * Theme functions.
 *
 * @todo t('', array(), $language); // add user langage.
 * @todo format_plural($count, '', '', array(), $language); // add user langage.
 */

/**
 * Theme introduction to an outgoing messaging call.
 *
 * @ingroup themeable.
 *
 * @return
 *   String text to be said.
 */
function theme_messaging_voip_message_send_intro($account = NULL) {
  $name = is_object($account) ? $account->name : '';
  $text = t('Hello @name, this is a message from the website @sitename', array('@name' => $name, '@sitename' => variable_get('site_name', 'Drupal')));
  $text .= t('You have a message.');

  return $text;
}

/**
 * Theme end to an outgoing messaging call.
 *
 * @ingroup themeable.
 *
 * @return
 *   String text to be said.
 */
function theme_messaging_voip_message_send_end() {
  $text = t('Goodbye');

  return $text;
}

/**
 * Theme the messaging store index. 
 *
 * Read out if someone calls up for their messages (and there are some),
 * or if they have been called with messages in the queue.
 * 
 * @todo theme the options?
 *
 * @ingroup themeable.
 *
 * @param $count
 *   Int number of messages.
 * @param $messages
 *   Array of messages, useful for example if you want to read out titles.
 * @return
 *   String to be read out.
 */
function theme_messaging_voip_store_index($count, $messages) {
  $text = format_plural($count, 'There is one message', 'There are @count messages');
  return $text;
}

/**
 * Theme no messages pending.
 *
 * Read out if someone has called up and there are no messages in the queue.
 *
 * @ingroup themeable.
 *
 * @return
 *   String to be read out.
 */
function theme_messaging_voip_store_nomessages() {
  // @todo probably to trivial, can be in _script_
  $text = t('There are no notifications waiting');

  return $text;
}
